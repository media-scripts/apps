#!/usr/bin/env python3

import requests
import json

### Configs Section
### Each notification channel is optional, but you should configure at least one
### UI products in the product handle taken from the product url

## Discord config
discord_webhook = '' #Discord webhook

## Slack Webhook
slack_webhook = '' # Slack webhook

##Twilio Config
account_sid = '' # twilio account sid https://support.twilio.com/hc/en-us/articles/223136607-What-is-an-Application-SID-
auth_token = '' # twilio auth token https://www.twilio.com/docs/iam/access-tokens
twilio_number = '+12348109314' # twilio phone number given to you
text_to_number = [ '+1XXXXXXXXXX' ] # replace with your cell number, can be a list of numbers to send to

##UI Products to check
product_handles = [ 'udm-pro', 'uvc-g4-doorbell' ]

######## No Config below this ########

def checkStatus():
# Get status of product
    page = requests.get('https://store.ui.com/products.json')
    products = json.loads(page.text)['products']

    for handle in product_handles:
        for product in products:
            if handle in product['handle'] and product['variants'][0]['available'] == True:
                if discord_webhook:
                    sendDiscord(product)
                if slack_webhook:
                    sendSlack(product)
                if account_sid:
                    sendTwilio(product)
                if not (discord_webhook) and not (account_sid):
                    print("No notifications enabled!")


def sendTwilio(data):
    from twilio.rest import Client

    client = Client(account_sid, auth_token)
    for number in text_to_number:
        message = client.messages \
            .create(
                body="%s is now in-stock!\nhttps://store.ui.com/products/%s" % (data['title'], data['handle']),
                from_=twilio_number,
                to=number,
                         )


def sendDiscord(data):
    from discord import Webhook, RequestsWebhookAdapter

    dis_id = int(discord_webhook.split('/')[5])
    dis_token = discord_webhook.split('/')[6]
    webhook = Webhook.partial(dis_id, dis_token, adapter=RequestsWebhookAdapter())
    webhook.send("%s is now in-stock!\nhttps://store.ui.com/products/%s" % (data['title'], data['handle']), username='UI Stock Bot')

def sendSlack(data):
    from slack_webhook import Slack

    slack = Slack(url=slack_webhook)
    slack.post(text="%s is now in-stock!\nhttps://store.ui.com/products/%s" % (data['title'], data['handle']),
        username="UI Stock Bot",
        )

if __name__ == '__main__':
    checkStatus()