#!/usr/bin/env python3

## This script will transfer files from one directory to another
## and fill the drive to a specified percentage
## ./drive_fill.py -source <source dir> -dest <dest dir> -thresh 80 (fill to 80%)

import os
import argparse
import shutil
import distutils
from distutils import dir_util
from hurry.filesize import size
import random

parser = argparse.ArgumentParser(description='Randomly copies files from one \
                                 location to another.')
parser.add_argument('--source', help='Source directory to copy files from')
parser.add_argument('--dest', help='Destination directory to copy files to')
parser.add_argument('--perc', help='Percentage to fill the destination to', \
                     default='80', type=str)
parser.add_argument('--noholiday', help='No Holiday movies', action='store_true')
parser.add_argument('--verbose', help='More Verbose output', \
                     default='0', type=int)

args = parser.parse_args()
source = args.source
dest = args.dest
perc = args.perc
verbose = args.verbose


def calcPerc(perc, dest, source):
  if perc.endswith("%"):
    perc = perc.replace("%", "")
  perc = float(perc)/100
  findDestSize(perc, dest, source)


def findDestSize(perc, dest, source):
    total, used, free = shutil.disk_usage(dest)
    to_copy = total * perc
    if verbose == 1:
        print('Total:\t\t' + str(size(total)))
        print('Used:\t\t' + str(size(used)))
        print('Free:\t\t' + str(size(free)))
        print('Target Used:\t' + str(size(to_copy)))
    copyFiles(total, used, free, to_copy, perc, source)


def copyFiles(total, used, free, to_copy, perc, source):
    if used >= to_copy:
        print("copy complete")
    else:
        fileList = []
        # print(next(os.walk(source, topdown=True)))
        for root, dirs, files in os.walk(source, topdown=True):
          #for name in files:
              # fileList.append(os.path.join(root, name))
              # print(len(fileList))
              # print(os.path.join(root, name))
              # print(name)
          for name in dirs:
              if name != '_gsdata_':
                  if args.noholiday:
                      if name in ['Christmas', 'Holiday', 'Halloween', 'Easter', 'New Year', 'Noel']:
                          print("Skipping %s" % (name))
                      else:
                  #print(os.path.join(root, name))
                          fileList.append(os.path.join(root, name))
                  else:
                      fileList.append(os.path.join(root, name))
                  #print(len(fileList))
          #print(len(fileList))
          rando = random.choice(fileList)
          print(rando)
          new_dir = rando.split('/')[-1]
          distutils.dir_util.mkpath(dest + new_dir, dry_run=0, verbose=verbose)
          distutils.dir_util.copy_tree(rando, dest + new_dir)
          findDestSize(perc, dest, source)
          break


if __name__ == '__main__':
    calcPerc(perc, dest, source)
