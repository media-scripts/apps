#!/usr/bin/env python
import os
import sys
import logging
import requests
import json
from logging.config import fileConfig
import configparser


## Configs to be changed

configSection = '4kRadarr' ## [Section] in autoProcess.ini for new Radarr
qualityId = '5' ## Quality ID of the quality you want to use 5 is by default UltraHD

## Change nothing below this

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(sys.argv[0]), 'autoProcess.ini'))

fileConfig(os.path.join(os.path.dirname(sys.argv[0]), 'logging.ini'), defaults={'logfilename': os.path.join(os.path.dirname(sys.argv[0]), 'info.log')})
log = logging.getLogger("RadarrSync")

log.info("Radarr sync script started.")

log.debug(os.environ)

radarr_host = config[configSection]['host']
radarr_port = config[configSection]['port']
radarr_web_root = config[configSection]['web_root']
log.debug('Radarr Host: %s' % radarr_host)
log.debug('Radarr Port: %s' % radarr_port)
log.debug('Radarr Web Root: %s' % radarr_web_root)
tmdbid = os.environ.get('radarr_movie_tmdbid')

syncHost = "http://%s:%s%s/api" % (radarr_host, radarr_port, radarr_web_root)

payload = {'tmdbId': tmdbid,
           'apikey': config[configSection]['apikey']}

url = syncHost + '/movie/lookup/tmdb'
r = requests.get(url, params=payload)
log.info("GET Status Code: %s" % r.status_code)
log.debug("Request URL: %s" % r.url)
title = r.json()['title']
titleSlug = r.json()['titleSlug']
images = r.json()['images']
year = r.json()['year']

url = syncHost + '/rootfolder'
headers = {'X-Api-Key': config[configSection]['apikey']}
r = requests.get(url, headers=headers)
path = r.json()[0]['path'] + title.replace(':','') + ' (' + str(year) + ')'

log.debug("TMDB ID: %s." % tmdbid)
log.debug("Title: %s." % title)
log.debug("Quality ID: %s." % qualityId)
log.debug("Title Slug: %s." % titleSlug)
log.debug("Images: %s." % images)
log.debug("Year: %s." % year)
log.debug("Path: %s." % path)

headers = {'X-Api-Key': config[configSection]['apikey'],
           'content-type': 'application/json'}
log.debug(headers)
data = {'title':title,
             'qualityProfileId':qualityId,
             'titleSlug':titleSlug,
             'images':images,
             'tmdbId':tmdbid,
             'year':year,
             'path':path,
             'monitored':'true',
             'addOptions': {
               'searchForMovie':'true'
             },
            }

log.debug(data)
url = syncHost + '/movie'
p = requests.post(url, headers=headers, data=json.dumps(data))

log.info("POST Status Code: %s" % p.status_code)
if (p.status_code == 200) or (p.status_code == 201):
    log.info("Movie %s sync'd successfully!" % title)
else:
    log.info("Movie %s failed to sync." % title)
